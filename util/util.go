package util

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"cloud.google.com/go/bigquery"
	"cloud.google.com/go/iam"
	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

const (
	ProjectId       = "dataintegration-072018"
	AvailBucketName = "td-datahub"
	DataSetId       = "transfashion"
	TableId         = "transaction"
)

// struct for transfashion json
type Transfashion struct {
	PAYMENTS []PaymentTF `json:"PAYMENTS" bigquery:"PAYMENTS"`
	ITEMS    []ItemTF    `json:"ITEMS" bigquery:"ITEMS"`
}

type PaymentTF struct {
	Payment
	BONISVOID    int    `json:"BON_ISVOID" bigquery:"BON_ISVOID"`
	PAYMENTVALUE string `json:"PAYMENT_VALUE" bigquery:"PAYMENT_VALUE"`
	BONDATE      string `json:"BON_DATE" bigquery:"BON_DATE"`
	BONTIME      string `json:"BON_TIME" bigquery:"BON_TIME"`
}

type ItemTF struct {
	Item
	BONDATE              string `json:"BON_DATE" bigquery:"BON_DATE"`
	BONTIME              string `json:"BON_TIME" bigquery:"BON_TIME"`
	HEINVGROISCONSUMABLE int    `json:"HEINVGRO_ISCONSUMABLE" bigquery:"HEINVGRO_ISCONSUMABLE"`
	SALESQTY             string `json:"SALES_QTY" bigquery:"SALES_QTY"`
	SALESGROSS           string `json:"SALES_GROSS" bigquery:"SALES_GROSS"`
	SALESNETT            string `json:"SALES_NETT" bigquery:"SALES_NETT"`
}

// end of transfashion json struct

// transfashion struct for bigquery
type TransfashionBQ struct {
	PAYMENTS []PaymentBQ `json:"PAYMENTS" bigquery:"PAYMENTS"`
	ITEMS    []ItemBQ    `json:"ITEMS" bigquery:"ITEMS"`
}

type PaymentBQ struct {
	Payment
	BONISVOID    bool      `bigquery:"BON_ISVOID"`
	PAYMENTVALUE int       ` bigquery:"PAYMENT_VALUE"`
	BONTIMESTAMP time.Time ` bigquery:"BON_TIMESTAMP"`
}

type ItemBQ struct {
	Item
	HEINVGROISCONSUMABLE bool ` bigquery:"HEINVGRO_ISCONSUMABLE"`
	SALESQTY             int  ` bigquery:"SALES_QTY"`
	SALESGROSS           int  ` bigquery:"SALES_GROSS"`
	SALESNETT            int  ` bigquery:"SALES_NETT"`
}

// end of struct bigquery

// type ItemBQ struct {
// 	*Item
// 	BONTIMESTAMP time.Time `json:"BON_TIMESTAMP" bigquery:"BON_TIMESTAMP"`
// }

type Payment struct {
	BONID string `json:"BON_ID" bigquery:"BON_ID"`
	// BONDATE           string `json:"BON_DATE" bigquery:"BON_DATE"`
	// BONTIME           string `json:"BON_TIME" bigquery:"BON_TIME"`
	SITEID            string `json:"SITE_ID" bigquery:"SITE_ID"`
	SALESPERSONNIK    int    `json:"SALESPERSON_NIK" bigquery:"SALESPERSON_NIK"`
	SALESPERSONNAME   string `json:"SALESPERSON_NAME" bigquery:"SALESPERSON_NAME"`
	CUSTID            int    `json:"CUST_ID" bigquery:"CUST_ID"`
	CUSTNAME          string `json:"CUST_NAME" bigquery:"CUST_NAME"`
	BONDISCTYPE       string `json:"BON_DISCTYPE" bigquery:"BON_DISCTYPE"`
	POSPAYMENTID      string `json:"POSPAYMENT_ID" bigquery:"POSPAYMENT_ID"`
	POSPAYMENTNAME    string `json:"POSPAYMENT_NAME" bigquery:"POSPAYMENT_NAME"`
	PAYMENTCARDNUMBER string `json:"PAYMENT_CARDNUMBER" bigquery:"PAYMENT_CARDNUMBER"`
	PAYMENTCARDHOLDER string `json:"PAYMENT_CARDHOLDER" bigquery:"PAYMENT_CARDHOLDER"`
}

type Item struct {
	BONID string `json:"BON_ID" bigquery:"BON_ID"`
	// BONDATE              string `json:"BON_DATE" bigquery:"BON_DATE"`
	// BONTIME              string `json:"BON_TIME" bigquery:"BON_TIME"`
	SITEID        string `json:"SITE_ID" bigquery:"SITE_ID"`
	UNITID        string `json:"UNIT_ID" bigquery:"UNIT_ID"`
	HEINVID       string `json:"HEINV_ID" bigquery:"HEINV_ID"`
	HEINVART      string `json:"HEINV_ART" bigquery:"HEINV_ART"`
	HEINVMAT      string `json:"HEINV_MAT" bigquery:"HEINV_MAT"`
	HEINVCOL      string `json:"HEINV_COL" bigquery:"HEINV_COL"`
	HEINVSIZE     string `json:"HEINV_SIZE" bigquery:"HEINV_SIZE"`
	HEINVNAME     string `json:"HEINV_NAME" bigquery:"HEINV_NAME"`
	HEINVCTGCLASS string `json:"HEINVCTG_CLASS" bigquery:"HEINVCTG_CLASS"`
	HEINVCTGNAME  string `json:"HEINVCTG_NAME" bigquery:"HEINVCTG_NAME"`
	HEINVCTGID    string `json:"HEINVCTG_ID" bigquery:"HEINVCTG_ID"`
	HEINVGRONAME  string `json:"HEINVGRO_NAME" bigquery:"HEINVGRO_NAME"`

	HEINVGROID      string `json:"HEINVGRO_ID" bigquery:"HEINVGRO_ID"`
	SEASONID        string `json:"SEASON_ID" bigquery:"SEASON_ID"`
	SALESPERSONNIK  int    `json:"SALESPERSON_NIK" bigquery:"SALESPERSON_NIK"`
	SALESPERSONNAME string `json:"SALESPERSON_NAME" bigquery:"SALESPERSON_NAME"`
	CUSTID          int    `json:"CUST_ID" bigquery:"CUST_ID"`
	CUSTNAME        string `json:"CUST_NAME" bigquery:"CUST_NAME"`
	BONDISCTYPE     string `json:"BON_DISCTYPE" bigquery:"BON_DISCTYPE"`
}

func Write(client *storage.Client, bucket, object string, data []byte) error {
	ctx := context.Background()

	r := bytes.NewReader(data)

	wc := client.Bucket(bucket).Object(object).NewWriter(ctx)
	if _, err := io.Copy(wc, r); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}
	// [END upload_file]
	return nil
}

func List(client *pubsub.Client) ([]*pubsub.Subscription, error) {
	ctx := context.Background()
	// [START pubsub_list_subscriptions]
	var subs []*pubsub.Subscription
	it := client.Subscriptions(ctx)
	for {
		s, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		subs = append(subs, s)
	}
	// [END pubsub_list_subscriptions]
	return subs, nil
}

func PullMsgs(client *pubsub.Client, subName string, topic *pubsub.Topic) error {
	ctx := context.Background()

	bigQueryClient, err := bigquery.NewClient(ctx, ProjectId)

	// [START pubsub_subscriber_async_pull]
	// [START pubsub_quickstart_subscriber]
	// Consume 10 messages.
	var mu sync.Mutex
	received := 0
	sub := client.Subscription(subName)
	cctx, _ := context.WithCancel(ctx)
	err = sub.Receive(cctx, func(ctx context.Context, msg *pubsub.Message) {
		msg.Ack()
		// fmt.Printf("Got message: %q\n", string(msg.Data))
		// log.Print(msg.Data)
		err = WriteMessageToGCS(msg.Data)
		if err != nil {
			log.Print(err)
		}
		// log.Print(string(msg.Data))
		err = WriteIntoBigQuery(bigQueryClient, DataSetId, TableId, msg.Data)
		if err != nil {
			log.Print(err)
		}
		mu.Lock()
		defer mu.Unlock()
		received++
		// if received == 10 {
		// 	cancel()
		// }
	})
	if err != nil {
		return err
	}

	// [END pubsub_subscriber_async_pull]
	// [END pubsub_quickstart_subscriber]
	// return nil
	return err
}

func Create(client *pubsub.Client, subName string, topic *pubsub.Topic) error {
	ctx := context.Background()
	// [START pubsub_create_pull_subscription]
	sub, err := client.CreateSubscription(ctx, subName, pubsub.SubscriptionConfig{
		Topic:       topic,
		AckDeadline: 20 * time.Second,
	})
	if err != nil {
		return err
	}
	fmt.Printf("Created subscription: %v\n", sub)
	// [END pubsub_create_pull_subscription]
	return nil
}

func CreateTopicIfNotExists(c *pubsub.Client) *pubsub.Topic {
	ctx := context.Background()

	const topic = "transfashion-topic"
	// Create a topic to subscribe to.
	t := c.Topic(topic)
	ok, err := t.Exists(ctx)
	if err != nil {
		log.Fatal(err)
	}
	if ok {
		return t
	}

	t, err = c.CreateTopic(ctx, topic)
	if err != nil {
		log.Fatalf("Failed to create the topic: %v", err)
	}
	return t
}

func WriteMessageToGCS(b []byte) error {
	var err error
	var transFashion Transfashion
	var fileName string
	now := time.Now().Format("01-02-2006")
	dateSplit := strings.Split(now, "-")
	day := dateSplit[1]
	month := dateSplit[0]
	year := dateSplit[2]

	json.Unmarshal(b, &transFashion)
	if len(b) > 0 {
		fileName = transFashion.PAYMENTS[0].BONID
	}
	// fileName := time.Now().Format("20060102150405")
	bucket := AvailBucketName
	object := fmt.Sprintf("transfashion/transaction/%v/%v/%v/%v.json", year, month, day, fileName)
	// object := "transfashion/testing/testingaja.json"
	ctx := context.Background()
	client, _ := storage.NewClient(ctx)
	// data := []byte(s)
	if err := Write(client, bucket, object, b); err != nil {
		log.Fatalf("Cannot write object: %v", err)
	}
	return err
}

func WriteIntoBigQuery(client *bigquery.Client, datasetID, tableID string, data []byte) error {
	ctx := context.Background()
	var err error
	// [CREATE SCHEMA]
	// err := createTableInferredSchema(client, datasetID, tableID)
	// if err != nil {
	// 	log.Print(err)
	// }

	// [START bigquery_table_insert_rows]
	u := client.Dataset(datasetID).Table(tableID).Inserter()
	TFBQ := ConvertToTransfashionBQ(data)
	err = u.Put(ctx, TFBQ)
	if err != nil {
		log.Print(err)
		if pme, ok := err.(bigquery.PutMultiError); ok {
			for _, e := range pme {
				log.Println(e)
			}
		}
		return err
	}
	msg := "get data from transfashion publisher"
	timeNow := time.Now()
	logMsg := fmt.Sprintf("%v on %v", msg, timeNow)
	log.Println(logMsg)
	// [END bigquery_table_insert_rows]
	return nil
}

func createTableInferredSchema(client *bigquery.Client, datasetID, tableID string) error {
	ctx := context.Background()

	sampleSchema := bigquery.Schema{
		{Name: "PAYMENTS",
			Type:     bigquery.RecordFieldType,
			Repeated: true,
			Schema: bigquery.Schema{
				{Name: "BON_ID", Type: bigquery.StringFieldType},
				{Name: "BON_TIMESTAMP", Type: bigquery.TimestampFieldType},
				// {Name: "BON_TIME", Type: bigquery.StringFieldType},
				{Name: "BON_ISVOID", Type: bigquery.BooleanFieldType},
				{Name: "SITE_ID", Type: bigquery.StringFieldType},
				{Name: "SALESPERSON_NIK", Type: bigquery.IntegerFieldType},
				{Name: "SALESPERSON_NAME", Type: bigquery.StringFieldType},
				{Name: "CUST_ID", Type: bigquery.IntegerFieldType},
				{Name: "CUST_NAME", Type: bigquery.StringFieldType},
				{Name: "BON_DISCTYPE", Type: bigquery.StringFieldType},
				{Name: "POSPAYMENT_ID", Type: bigquery.StringFieldType},
				{Name: "POSPAYMENT_NAME", Type: bigquery.StringFieldType},
				{Name: "PAYMENT_CARDNUMBER", Type: bigquery.StringFieldType},
				{Name: "PAYMENT_CARDHOLDER", Type: bigquery.StringFieldType},
				{Name: "PAYMENT_VALUE", Type: bigquery.IntegerFieldType},
			}},
		{Name: "ITEMS",
			Type:     bigquery.RecordFieldType,
			Repeated: true,
			Schema: bigquery.Schema{
				{Name: "BON_ID", Type: bigquery.StringFieldType},
				// {Name: "BON_DATE", Type: bigquery.TimestampFieldType},
				// {Name: "BON_TIME", Type: bigquery.StringFieldType},
				{Name: "SITE_ID", Type: bigquery.StringFieldType},
				{Name: "UNIT_ID", Type: bigquery.StringFieldType},
				{Name: "HEINV_ID", Type: bigquery.StringFieldType},
				{Name: "HEINV_ART", Type: bigquery.StringFieldType},
				{Name: "HEINV_MAT", Type: bigquery.StringFieldType},
				{Name: "HEINV_COL", Type: bigquery.StringFieldType},
				{Name: "HEINV_SIZE", Type: bigquery.StringFieldType},
				{Name: "HEINV_NAME", Type: bigquery.StringFieldType},
				{Name: "HEINVCTG_CLASS", Type: bigquery.StringFieldType},
				{Name: "HEINVCTG_NAME", Type: bigquery.StringFieldType},
				{Name: "HEINVCTG_ID", Type: bigquery.StringFieldType},
				{Name: "HEINVGRO_NAME", Type: bigquery.StringFieldType},
				{Name: "HEINVGRO_ISCONSUMABLE", Type: bigquery.BooleanFieldType},
				{Name: "HEINVGRO_ID", Type: bigquery.StringFieldType},
				{Name: "SEASON_ID", Type: bigquery.StringFieldType},
				{Name: "SALESPERSON_NIK", Type: bigquery.IntegerFieldType},
				{Name: "SALESPERSON_NAME", Type: bigquery.StringFieldType},
				{Name: "CUST_ID", Type: bigquery.IntegerFieldType},
				{Name: "CUST_NAME", Type: bigquery.StringFieldType},
				{Name: "BON_DISCTYPE", Type: bigquery.StringFieldType},
				{Name: "SALES_QTY", Type: bigquery.IntegerFieldType},
				{Name: "SALES_GROSS", Type: bigquery.IntegerFieldType},
				{Name: "SALES_NETT", Type: bigquery.IntegerFieldType},
			}},
	}

	metaData := &bigquery.TableMetadata{
		Schema: sampleSchema,
	}
	tableRef := client.Dataset(datasetID).Table(tableID)
	if err := tableRef.Create(ctx, metaData); err != nil {
		return err
	}
	fmt.Printf("created table %s\n", tableRef.FullyQualifiedName())
	// // bigquery.InferSchema infers BQ schema from native Go types.
	// schema, err := bigquery.InferSchema(Transfashion{})
	// if err != nil {
	// 	return err
	// }
	// table := client.Dataset(datasetID).Table(tableID)
	// if err := table.Create(ctx, &bigquery.TableMetadata{Schema: schema}); err != nil {
	// 	return err
	// }
	return nil
}

func ConvertToTransfashionBQ(data []byte) TransfashionBQ {
	var res TransfashionBQ
	var temp Transfashion
	// var data2 []byte
	// data2 = data
	json.Unmarshal(data, &temp)
	// log.Println(temp.PAYMENTS[0].BONID)
	// json.Unmarshal(data2, &res)
	// log.Println(res.PAYMENTS[0].BONID)
	// log.Println(unsafe.Sizeof(res))
	// log.Println(unsafe.Sizeof(temp))
	var arrayTempPayment []PaymentBQ
	var arrayTempItem []ItemBQ
	for j, _ := range temp.PAYMENTS {
		var tempPayment PaymentBQ
		dateSplit := strings.Split(temp.PAYMENTS[j].BONDATE, "-")
		timeSplit := strings.Split(temp.PAYMENTS[j].BONTIME, "-")
		year := dateSplit[0]
		month := dateSplit[1]
		date := dateSplit[2]
		hour := timeSplit[0]
		min := timeSplit[1]
		sec := timeSplit[2]

		timeFormat := fmt.Sprintf("%v-%v-%vT%v:%v:%v+00:00", year, month, date, hour, min, sec)
		timestamp, _ := time.Parse(time.RFC3339, timeFormat)

		tempPayment.BONID = temp.PAYMENTS[j].BONID
		tempPayment.BONTIMESTAMP = timestamp
		tempPayment.SITEID = temp.PAYMENTS[j].SITEID
		tempPayment.SALESPERSONNIK = temp.PAYMENTS[j].SALESPERSONNIK
		tempPayment.SALESPERSONNAME = temp.PAYMENTS[j].SALESPERSONNAME
		tempPayment.CUSTID = temp.PAYMENTS[j].CUSTID
		tempPayment.CUSTNAME = temp.PAYMENTS[j].CUSTNAME
		tempPayment.BONDISCTYPE = temp.PAYMENTS[j].BONDISCTYPE
		tempPayment.POSPAYMENTID = temp.PAYMENTS[j].POSPAYMENTID
		tempPayment.POSPAYMENTNAME = temp.PAYMENTS[j].POSPAYMENTNAME
		tempPayment.PAYMENTCARDNUMBER = temp.PAYMENTS[j].PAYMENTCARDNUMBER
		tempPayment.PAYMENTCARDHOLDER = temp.PAYMENTS[j].PAYMENTCARDHOLDER
		tempPayment.PAYMENTVALUE, _ = strconv.Atoi(temp.PAYMENTS[j].PAYMENTVALUE)
		arrayTempPayment = append(arrayTempPayment, tempPayment)
	}
	for k, _ := range temp.ITEMS {
		var tempItem ItemBQ
		tempItem.BONID = temp.ITEMS[k].BONID
		tempItem.UNITID = temp.ITEMS[k].UNITID
		tempItem.HEINVID = temp.ITEMS[k].HEINVID
		tempItem.HEINVART = temp.ITEMS[k].HEINVART
		tempItem.HEINVMAT = temp.ITEMS[k].HEINVMAT
		tempItem.HEINVCOL = temp.ITEMS[k].HEINVCOL
		tempItem.HEINVSIZE = temp.ITEMS[k].HEINVSIZE
		tempItem.HEINVNAME = temp.ITEMS[k].HEINVNAME
		tempItem.HEINVCTGCLASS = temp.ITEMS[k].HEINVCTGCLASS
		tempItem.HEINVCTGNAME = temp.ITEMS[k].HEINVCTGNAME
		tempItem.HEINVCTGID = temp.ITEMS[k].HEINVCTGID
		tempItem.HEINVGRONAME = temp.ITEMS[k].HEINVGRONAME
		tempItem.HEINVGROISCONSUMABLE = ConvertIntToBool(temp.ITEMS[k].HEINVGROISCONSUMABLE)
		tempItem.HEINVGROID = temp.ITEMS[k].HEINVGROID
		tempItem.SEASONID = temp.ITEMS[k].SEASONID
		tempItem.SALESPERSONNIK = temp.ITEMS[k].SALESPERSONNIK
		tempItem.SALESPERSONNAME = temp.ITEMS[k].SALESPERSONNAME
		tempItem.SALESQTY, _ = strconv.Atoi(temp.ITEMS[k].SALESQTY)
		tempItem.SALESGROSS, _ = strconv.Atoi(temp.ITEMS[k].SALESGROSS)
		tempItem.SALESNETT, _ = strconv.Atoi(temp.ITEMS[k].SALESNETT)
		arrayTempItem = append(arrayTempItem, tempItem)
	}
	res.PAYMENTS = arrayTempPayment
	res.ITEMS = arrayTempItem
	// log.Println(res)
	return res

}

func ConvertIntToBool(i int) bool {
	if i == 1 {
		return true
	}
	return false
}

/*

lines below used for pubsub function

right now not used yet


*/

func PullMsgsError(client *pubsub.Client, subName string) error {
	ctx := context.Background()
	// [START pubsub_subscriber_error_listener]
	// If the service returns a non-retryable error, Receive returns that error after
	// all of the outstanding calls to the handler have returned.
	err := client.Subscription(subName).Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		fmt.Printf("Got message: %q\n", string(msg.Data))
		msg.Ack()
	})
	if err != nil {
		return err
	}
	// [END pubsub_subscriber_error_listener]
	return nil
}

func pullMsgsSettings(client *pubsub.Client, subName string) error {
	ctx := context.Background()
	// [START pubsub_subscriber_flow_settings]
	sub := client.Subscription(subName)
	sub.ReceiveSettings.MaxOutstandingMessages = 10
	err := sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		fmt.Printf("Got message: %q\n", string(msg.Data))
		msg.Ack()
	})
	if err != nil {
		return err
	}
	// [END pubsub_subscriber_flow_settings]
	return nil
}

func createWithEndpoint(client *pubsub.Client, subName string, topic *pubsub.Topic, endpoint string) error {
	ctx := context.Background()
	// [START pubsub_create_push_subscription]

	// For example, endpoint is "https://my-test-project.appspot.com/push".
	sub, err := client.CreateSubscription(ctx, subName, pubsub.SubscriptionConfig{
		Topic:       topic,
		AckDeadline: 10 * time.Second,
		PushConfig:  pubsub.PushConfig{Endpoint: endpoint},
	})
	if err != nil {
		return err
	}
	fmt.Printf("Created subscription: %v\n", sub)
	// [END pubsub_create_push_subscription]
	return nil
}

func updateEndpoint(client *pubsub.Client, subName string, endpoint string) error {
	ctx := context.Background()
	// [START pubsub_update_push_configuration]

	// For example, endpoint is "https://my-test-project.appspot.com/push".
	subConfig, err := client.Subscription(subName).Update(ctx, pubsub.SubscriptionConfigToUpdate{
		PushConfig: &pubsub.PushConfig{Endpoint: endpoint},
	})
	if err != nil {
		return err
	}
	fmt.Printf("Updated subscription config: %#v", subConfig)
	// [END pubsub_update_push_configuration]
	return nil
}

func delete(client *pubsub.Client, subName string) error {
	ctx := context.Background()
	// [START pubsub_delete_subscription]
	sub := client.Subscription(subName)
	if err := sub.Delete(ctx); err != nil {
		return err
	}
	fmt.Println("Subscription deleted.")
	// [END pubsub_delete_subscription]
	return nil
}

func getPolicy(c *pubsub.Client, subName string) (*iam.Policy, error) {
	ctx := context.Background()

	// [START pubsub_get_subscription_policy]
	policy, err := c.Subscription(subName).IAM().Policy(ctx)
	if err != nil {
		return nil, err
	}
	for _, role := range policy.Roles() {
		log.Printf("%q: %q", role, policy.Members(role))
	}
	// [END pubsub_get_subscription_policy]
	return policy, nil
}

func addUsers(c *pubsub.Client, subName string) error {
	ctx := context.Background()

	// [START pubsub_set_subscription_policy]
	sub := c.Subscription(subName)
	policy, err := sub.IAM().Policy(ctx)
	if err != nil {
		return err
	}
	// Other valid prefixes are "serviceAccount:", "user:"
	// See the documentation for more values.
	policy.Add(iam.AllUsers, iam.Viewer)
	policy.Add("group:cloud-logs@google.com", iam.Editor)
	if err := sub.IAM().SetPolicy(ctx, policy); err != nil {
		return err
	}
	// NOTE: It may be necessary to retry this operation if IAM policies are
	// being modified concurrently. SetPolicy will return an error if the policy
	// was modified since it was retrieved.
	// [END pubsub_set_subscription_policy]
	return nil
}

func testPermissions(c *pubsub.Client, subName string) ([]string, error) {
	ctx := context.Background()

	// [START pubsub_test_subscription_permissions]
	sub := c.Subscription(subName)
	perms, err := sub.IAM().TestPermissions(ctx, []string{
		"pubsub.subscriptions.consume",
		"pubsub.subscriptions.update",
	})
	if err != nil {
		return nil, err
	}
	for _, perm := range perms {
		log.Printf("Allowed: %v", perm)
	}
	// [END pubsub_test_subscription_permissions]
	return perms, nil
}
